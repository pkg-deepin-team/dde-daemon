# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Ldm Public <ldmpub@gmail.com>, 2020
# FFXP <jeremy.pfenning@ironmail.fr>, 2020
# Neltus <neltus1@gmail.com>, 2020
# zwpwjwtz <zwpwjwtz@126.com>, 2020
# Aestan <anthony.margerand@protonmail.com>, 2020
# Joselito 8, 2020
# Charles Monzat <c.monzat@laposte.net>, 2020
# Tristan Denni <tristan.denni@viacesi.fr>, 2020
# Sami G.-D., 2020
# Kristien <kristien@mailo.com>, 2020
# Jean-Philippe Paumier <paumierj@hotmail.com>, 2020
# Cindy0514, 2020
# n1coc4cola <n1coc4cola@gmail.com>, 2020
# ba56da5c479245883255d97580e861b5_327de87 <cc1517672f7d7ecc8d2a76b655e9f30b_134975>, 2020
# roxfr <roxfr@outlook.fr>, 2021
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-04-23 13:23+0800\n"
"PO-Revision-Date: 2020-05-29 07:18+0000\n"
"Last-Translator: roxfr <roxfr@outlook.fr>, 2021\n"
"Language-Team: French (https://www.transifex.com/linuxdeepin/teams/3617/fr/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fr\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: ../../accounts/checkers/password.go:53
msgid "Please enter a password not less than 8 characters"
msgstr "Veuillez insérer un mot de passe d'au moins 8 caractères"

#: ../../accounts/checkers/password.go:55
msgid ""
"The password must contain English letters (case-sensitive), numbers or "
"special symbols (~!@#$%^&*()[]{}\\|/?,.<>)"
msgstr ""
"Le mot de passe doit contenir des lettres (sensibles à la casse), des "
"chiffres ou des symboles spéciaux (~!@#$%^&*()[]{}\\|/?,.<>)"

#: ../../accounts/checkers/username.go:64
msgid "Username cannot be empty"
msgstr "Le nom d'utilisateur ne peut pas être vide"

#: ../../accounts/checkers/username.go:69
msgid "The first character must be a letter or number"
msgstr "Le premier caractère doit être une lettre ou un chiffre"

#: ../../accounts/checkers/username.go:71
msgid "The username already exists"
msgstr "Ce nom d'utilisateur existe déjà"

#: ../../accounts/checkers/username.go:74
msgid "The name already exists"
msgstr "Le nom existe déjà"

#: ../../accounts/checkers/username.go:76
msgid "The username has been used by system"
msgstr "Le nom d'utilisateur a été utilisé par le système"

#: ../../accounts/checkers/username.go:78
msgid "Username must be between 3 and 32 characters"
msgstr "Le nom d'utilisateur doit être entre 3 et 32 caractères"

#: ../../appearance/scale.go:56
msgid "Log out for display scaling settings to take effect"
msgstr ""
"Déconnectez-vous pour que les paramètres de mise à l'échelle de l'affichage "
"prennent effet"

#: ../../appearance/scale.go:57
msgid "Set successfully"
msgstr "Établit avec succès"

#: ../../appearance/scale.go:67
msgid "Later"
msgstr "Plus tard"

#: ../../appearance/scale.go:67
msgid "Log Out Now"
msgstr "Déconnectez-vous maintenant"

#: ../../appearance/scale.go:81
msgid "Setting display scaling"
msgstr "Mise à l'échelle de l'affichage"

#: ../../appearance/scale.go:82
msgid "Display scaling"
msgstr "Échelle d'affichage"

#: ../../bin/dde-authority/fprint_transaction.go:21
msgid "Fingerprint verification failed"
msgstr "Echec de la vérification de l'empreinte digitale"

#: ../../bin/dde-authority/fprint_transaction.go:22
msgid "Fingerprint verification timed out"
msgstr "Expiration de la vérification de l'empreinte digitale"

#: ../../bluetooth/bluez_profile.go:30
msgid "Serial port"
msgstr "Port série"

#: ../../bluetooth/bluez_profile.go:31
msgid "Dial-Up networking"
msgstr "Réseau modem"

#: ../../bluetooth/bluez_profile.go:32
msgid "Hands-Free device"
msgstr "Kit main libre"

#: ../../bluetooth/bluez_profile.go:33
msgid "Hands-Free voice gateway"
msgstr "Passerelle de kit main libre"

#: ../../bluetooth/bluez_profile.go:34
msgid "Headset voice gateway"
msgstr "Passerelle pour micro-casque"

#: ../../bluetooth/bluez_profile.go:35
msgid "Object push"
msgstr "Poussage d'objet"

#: ../../bluetooth/bluez_profile.go:36
msgid "File transfer"
msgstr "Transfert de fichier"

#: ../../bluetooth/bluez_profile.go:37
msgid "Synchronization"
msgstr "Synchronisation"

#: ../../bluetooth/bluez_profile.go:38
msgid "Phone book access"
msgstr "Accès aux contacts téléphoniques"

#: ../../bluetooth/bluez_profile.go:39
msgid "Phone book access client"
msgstr "Client d'accès au contacts téléphoniques"

#: ../../bluetooth/bluez_profile.go:40
msgid "Message access"
msgstr "Message d'accès"

#: ../../bluetooth/bluez_profile.go:41
msgid "Message notification"
msgstr "Message de notification"

#: ../../bluetooth/obex_agent.go:339 ../../lastore/lastore.go:252
msgid "Cancel"
msgstr "Annuler"

#: ../../bluetooth/obex_agent.go:346
msgid "Receiving %q files from %q"
msgstr "Réception des fichiers %q de %q"

#: ../../bluetooth/obex_agent.go:355
msgid "View"
msgstr "Affichage"

#: ../../bluetooth/obex_agent.go:361
msgid "You have received files from %q successfully"
msgstr "Vous avez reçu les fichiers de %q avec succès"

#: ../../bluetooth/obex_agent.go:362
msgid "Done"
msgstr "Terminé"

#: ../../bluetooth/obex_agent.go:377 ../../bluetooth/obex_agent.go:476
msgid "Stop Receiving Files"
msgstr "Arrêter de recevoir des fichiers"

#: ../../bluetooth/obex_agent.go:379
msgid "You have cancelled the file transfer"
msgstr "Vous avez annulé le transfert de fichier"

#: ../../bluetooth/obex_agent.go:381 ../../bluetooth/utils_notify.go:113
#: ../../bluetooth/utils_notify.go:118
msgid "Bluetooth connection failed"
msgstr "Connection bluetooth échouée "

#: ../../bluetooth/obex_agent.go:416
msgid "Decline"
msgstr "Décliner"

#: ../../bluetooth/obex_agent.go:416
msgid "Receive"
msgstr "Recevoir"

#: ../../bluetooth/obex_agent.go:421
msgid "Bluetooth File Transfer"
msgstr "Transfert de fichiers par Bluetooth"

#: ../../bluetooth/obex_agent.go:422
msgid "%q wants to send files to you. Receive?"
msgstr "%q veut vous envoyer des fichiers. Accepter ?"

#: ../../bluetooth/obex_agent.go:477
msgid "Receiving %q timed out"
msgstr "Réception de %q a expiré"

#: ../../bluetooth/utils_notify.go:98
msgid "Connect %q successfully"
msgstr "%q connecté avec succès"

#: ../../bluetooth/utils_notify.go:103 ../../network/state_handler.go:335
#: ../../network/state_handler.go:357
msgid "%q disconnected"
msgstr "%q déconnecté"

#: ../../bluetooth/utils_notify.go:108
msgid "Make sure %q is turned on and in range"
msgstr "Assurez-vous que %q est sous tension "

#: ../../bluetooth/utils_notify.go:117
msgid ""
"%q can no longer connect to %q. Try to forget this device and pair it again."
msgstr ""
"%q ne peut plus se connecter à %q. Essayer d'oublier cet appareil et "
"associer-le à nouveau. "

#: ../../bluetooth/utils_notify.go:151
msgid "Click here to connect to %q"
msgstr "Cliquez ici pour vous connecter à %q"

#: ../../bluetooth/utils_notify.go:152
msgid "Add Bluetooth devices"
msgstr "Ajouter un appareil Bluetooth"

#: ../../bluetooth/utils_notify.go:162
msgid "Pair"
msgstr "Jumeler"

#: ../../calendar/job.go:509 ../../calendar/job.go:516
msgid "today"
msgstr "aujourd'hui"

#: ../../calendar/job.go:511 ../../calendar/job.go:518
msgid "tomorrow"
msgstr "demain"

#: ../../calendar/job.go:531
#, c-format
msgid "%s to %s"
msgstr "%s à %s"

#: ../../calendar/module.go:126 ../../calendar/type.go:25
msgid "Work"
msgstr "Travail"

#: ../../calendar/module.go:127 ../../calendar/type.go:30
msgid "Life"
msgstr "Vie"

#: ../../calendar/module.go:128 ../../calendar/type.go:35
msgid "Other"
msgstr "Autre"

#: ../../calendar/scheduler.go:570
msgid "One day before start"
msgstr "Un jour avant le début"

#: ../../calendar/scheduler.go:571 ../../calendar/scheduler.go:576
#: ../../calendar/scheduler.go:584 ../../calendar/scheduler.go:588
msgid "Close"
msgstr "Fermer"

#: ../../calendar/scheduler.go:575
msgid "Remind me tomorrow"
msgstr "Rappeler-le moi demain"

#: ../../calendar/scheduler.go:583
msgid "Remind me later"
msgstr "Rappeler-le moi plus tard"

#: ../../calendar/scheduler.go:593
msgid "Schedule Reminder"
msgstr "Rappel d'horaire"

#: ../../calendar/type.go:40
msgid "Festival"
msgstr "Festival"

#: ../../dock/app_entry_menu.go:102
msgid "Open"
msgstr "Ouvrir"

#: ../../dock/app_entry_menu.go:109
msgid "Close All"
msgstr "Fermer tout"

#: ../../dock/app_entry_menu.go:125
msgid "Force Quit"
msgstr "Forcer à quitter"

#: ../../dock/app_entry_menu.go:135
msgid "Dock"
msgstr "Dock"

#: ../../dock/app_entry_menu.go:145
msgid "Undock"
msgstr "Détacher du dock"

#: ../../dock/app_entry_menu.go:155
msgid "All Windows"
msgstr "Toutes les fenêtres"

#: ../../housekeeping/init.go:98
msgid "Insufficient disk space, please clean up in time!"
msgstr "Espace disque insuffisant, nettoyer de temps en temps!"

#: ../../keybinding/shortcuts/id_name_map.go:28
msgid "Launcher"
msgstr "Lanceur"

#: ../../keybinding/shortcuts/id_name_map.go:29
msgid "Terminal"
msgstr "Terminal"

#: ../../keybinding/shortcuts/id_name_map.go:30
msgid "Screen Recorder"
msgstr "Enregistrer l'écran"

#: ../../keybinding/shortcuts/id_name_map.go:31
msgid "Lock screen"
msgstr "Verrouiller l'écran"

#: ../../keybinding/shortcuts/id_name_map.go:32
msgid "Show/Hide the dock"
msgstr "Afficher/Masquer le dock"

#: ../../keybinding/shortcuts/id_name_map.go:33
msgid "Shutdown interface"
msgstr "Interrompre l'interface"

#: ../../keybinding/shortcuts/id_name_map.go:34
msgid "Terminal Quake Window"
msgstr "Fenêtre du terminal Quake"

#: ../../keybinding/shortcuts/id_name_map.go:35
msgid "Screenshot"
msgstr "Capture d'écran"

#: ../../keybinding/shortcuts/id_name_map.go:36
msgid "Full screenshot"
msgstr "Capture plein écran"

#: ../../keybinding/shortcuts/id_name_map.go:37
msgid "Window screenshot"
msgstr "Capture d'écran de la fenêtre"

#: ../../keybinding/shortcuts/id_name_map.go:38
msgid "Delay screenshot"
msgstr "Délai de capture d'écran"

#: ../../keybinding/shortcuts/id_name_map.go:39
msgid "File manager"
msgstr "Gestionnaire de fichiers"

#: ../../keybinding/shortcuts/id_name_map.go:40
msgid "Disable Touchpad"
msgstr "Désactiver le pavé tactile"

#: ../../keybinding/shortcuts/id_name_map.go:41
msgid "Switch window effects"
msgstr "Basculer entre les effets de fenêtres"

#: ../../keybinding/shortcuts/id_name_map.go:42
msgid "Fast Screen Off"
msgstr "Extinction d'écran rapide"

#: ../../keybinding/shortcuts/id_name_map.go:43
msgid "System Monitor"
msgstr "Moniteur système"

#: ../../keybinding/shortcuts/id_name_map.go:44
msgid "Deepin Picker"
msgstr "Deepin Picker"

#: ../../keybinding/shortcuts/id_name_map.go:45
msgid "Desktop AI Assistant"
msgstr "Assistant IA de bureau"

#: ../../keybinding/shortcuts/id_name_map.go:46
msgid "Text to Speech"
msgstr "Texte vers discours"

#: ../../keybinding/shortcuts/id_name_map.go:47
msgid "Speech to Text"
msgstr "Discours vers texte"

#: ../../keybinding/shortcuts/id_name_map.go:48
msgid "Clipboard"
msgstr "Presse-papiers "

#: ../../keybinding/shortcuts/id_name_map.go:49
msgid "Translation"
msgstr "Traduction"

#: ../../keybinding/shortcuts/id_name_map.go:56
msgid "Switch Layout"
msgstr "Changer de périphérique "

#: ../../keybinding/shortcuts/id_name_map.go:75
msgid "Switch to left workspace"
msgstr "Basculer à l'espace de travail de gauche"

#: ../../keybinding/shortcuts/id_name_map.go:76
msgid "Switch to right workspace"
msgstr "Basculer sur l'espace de travail de droite"

#: ../../keybinding/shortcuts/id_name_map.go:77
msgid "Switch to upper workspace"
msgstr "Basculer à l'espace de travail supérieur"

#: ../../keybinding/shortcuts/id_name_map.go:78
msgid "Switch to lower workspace"
msgstr "Basculer sur l'espace de travail inférieur"

#: ../../keybinding/shortcuts/id_name_map.go:80
msgid "Switch similar windows"
msgstr "Basculer entre les fenêtres similaires"

#: ../../keybinding/shortcuts/id_name_map.go:81
msgid "Switch similar windows in reverse"
msgstr "Basculer entre les fenêtres similaires dans le sens inverse"

#: ../../keybinding/shortcuts/id_name_map.go:82
msgid "Switch windows"
msgstr "Basculer entre les fenêtres"

#: ../../keybinding/shortcuts/id_name_map.go:83
msgid "Switch windows in reverse"
msgstr "Alterner les fenêtres en sens inverse"

#: ../../keybinding/shortcuts/id_name_map.go:94
msgid "Show desktop"
msgstr "Afficher le bureau"

#: ../../keybinding/shortcuts/id_name_map.go:103
msgid "Maximize window"
msgstr "Agrandir la fenêtre"

#: ../../keybinding/shortcuts/id_name_map.go:104
msgid "Restore window"
msgstr "Restaurer la fenêtre"

#: ../../keybinding/shortcuts/id_name_map.go:106
msgid "Minimize window"
msgstr "Réduire la fenêtre"

#: ../../keybinding/shortcuts/id_name_map.go:107
msgid "Close window"
msgstr "Fermer la fenêtre"

#: ../../keybinding/shortcuts/id_name_map.go:108
msgid "Move window"
msgstr "Déplacer la fenêtre"

#: ../../keybinding/shortcuts/id_name_map.go:109
msgid "Resize window"
msgstr "Redimensionner la fenêtre"

#: ../../keybinding/shortcuts/id_name_map.go:124
msgid "Move to left workspace"
msgstr "Aller à l'espace de travail de gauche"

#: ../../keybinding/shortcuts/id_name_map.go:125
msgid "Move to right workspace"
msgstr "Aller à l'espace de travail de droite"

#: ../../keybinding/shortcuts/id_name_map.go:126
msgid "Move to upper workspace"
msgstr "Aller à l'espace de travail supérieur"

#: ../../keybinding/shortcuts/id_name_map.go:127
msgid "Move to lower workspace"
msgstr "Aller à l'espace de travail inférieur"

#: ../../keybinding/shortcuts/id_name_map.go:149
msgid "Display windows of all workspaces"
msgstr "Afficher tous les espaces de travail"

#: ../../keybinding/shortcuts/id_name_map.go:150
msgid "Display windows of current workspace"
msgstr "Afficher l'espace de travail actuel"

#: ../../keybinding/shortcuts/id_name_map.go:151
msgid "Display workspace"
msgstr "Afficher l'espace de travail"

#: ../../keybinding/shortcuts/id_name_map.go:214
msgid "Switch monitors"
msgstr "Changer de moniteur"

#: ../../langselector/locale.go:62
msgid "Authentication is required to switch language"
msgstr "L'authentification est requise pour changer la langue"

#: ../../langselector/locale.go:151
msgid ""
"Changing system language and installing the required language packages, "
"please wait..."
msgstr ""
"Changement de la langue du système et installation des composants "
"linguistiques en cours, veuillez patienter..."

#: ../../langselector/locale.go:152
msgid "Changing system language, please wait..."
msgstr "Changement de la langue du système en cours, veuillez patienter..."

#: ../../langselector/locale.go:153
msgid "System language changed, please log out and then log in"
msgstr ""
"Langue du système changée, veuillez-vous déconnecter puis vous reconnecter"

#: ../../langselector/locale.go:394
msgid "Failed to change system language, please try later"
msgstr ""
"Impossible de changer la langue du système, veuillez réessayer plus tard"

#: ../../lastore/lastore.go:244
msgid "Retry"
msgstr "Réessayer"

#: ../../lastore/lastore.go:266
msgid "Update Now"
msgstr "Mettre à jour maintenant"

#: ../../lastore/notify.go:96
msgid "%q installed successfully."
msgstr "%q a été installé avec succès."

#: ../../lastore/notify.go:99
msgid "%q failed to install."
msgstr "%q installation échouée."

#: ../../lastore/notify.go:107
msgid "Removed successfully"
msgstr "Supprimé avec succès"

#: ../../lastore/notify.go:109
msgid "Failed to remove the app"
msgstr "La surpression de l'app a échouée"

#: ../../lastore/notify.go:116
msgid ""
"In order to prevent automatic shutdown, please plug in for normal update."
msgstr ""
"Afin d'éviter l'arrêt automatique, veuillez brancher votre appareil pendant "
"l'actualisation."

#: ../../lastore/notify.go:121
msgid "Package cache wiped"
msgstr "Cache de paquets effacé"

#: ../../lastore/notify.go:126
msgid "New system edition available"
msgstr "Nouvelle édition du système disponible"

#: ../../launcher/manager_uninstall.go:71
msgid "%q removed successfully"
msgstr "%q désinstallé avec succès"

#: ../../launcher/manager_uninstall.go:73
msgid "Failed to uninstall %q"
msgstr "Échec de la désinstallation de %q"

#: ../../network/manager.go:326
msgid "Wired connection"
msgstr ""

#: ../../network/manager_active_conn.go:399
#: ../../network/manager_active_conn.go:404
msgid "None"
msgstr "Aucun"

#: ../../network/manager_active_conn.go:406
msgid "WEP 40/128-bit Key"
msgstr "WEP 40/128-bit Key"

#: ../../network/manager_active_conn.go:408
msgid "WPA/WPA2 Personal"
msgstr "WPA/WPA2 Personnel"

#: ../../network/manager_active_conn.go:416
msgid "TLS"
msgstr "TLS"

#: ../../network/manager_active_conn.go:418
msgid "MD5"
msgstr "MD5"

#: ../../network/manager_active_conn.go:420
msgid "LEAP"
msgstr "LEAP"

#: ../../network/manager_active_conn.go:422
msgid "FAST"
msgstr "FAST"

#: ../../network/manager_active_conn.go:424
msgid "Tunneled TLS"
msgstr "TLS par tunnel"

#: ../../network/manager_active_conn.go:426
msgid "Protected EAP"
msgstr "EAP protégé"

#: ../../network/manager_connection.go:295
msgid "Wired Connection"
msgstr "Connexion filaire"

#: ../../network/proxychains/utils_notify.go:45
msgid "Application proxy is set successfully"
msgstr "Le proxy d'application est défini avec succès"

#: ../../network/proxychains/utils_notify.go:45
#: ../../network/proxychains/utils_notify.go:48
#: ../../network/utils_notify.go:197 ../../network/utils_notify.go:201
#: ../../network/utils_notify.go:204
msgid "Network"
msgstr "Réseau"

#: ../../network/proxychains/utils_notify.go:48
msgid "Failed to set the application proxy"
msgstr "Échec de la configuration du proxy d'application"

#: ../../network/state_handler.go:38
msgid "Device state changed"
msgstr "L'état du périphérique a changé"

#: ../../network/state_handler.go:39
msgid "Device state changed, reason unknown"
msgstr "Changement de l'état du périphérique, raison inconnue"

#: ../../network/state_handler.go:40
msgid "The device is now managed"
msgstr "Le périphérique est désormais géré"

#: ../../network/state_handler.go:41
msgid "The device is no longer managed"
msgstr "Le périphérique n'est plus géré"

#: ../../network/state_handler.go:42
msgid "The device has not been ready for configuration"
msgstr "Le périphérique n'a pas été prêt pour la configuration"

#: ../../network/state_handler.go:43
msgid ""
"IP configuration could not be reserved (no available address, timeout, etc)"
msgstr ""
"La configuration IP ne peut être réservée (pas d'adresse disponible, délai "
"dépassé, etc.)"

#: ../../network/state_handler.go:44
msgid "The IP configuration is no longer valid"
msgstr "La configuration IP n'est plus valide"

#: ../../network/state_handler.go:45
msgid "Passwords were required but not provided"
msgstr "Des mots de passe sont requis mais n'ont pas été fournis"

#: ../../network/state_handler.go:46
msgid ""
"The 802.1X supplicant disconnected from the access point or authentication "
"server"
msgstr ""
"Le suppliant 802.1X s'est déconnecté du point d'accès ou du serveur "
"d'authentification"

#: ../../network/state_handler.go:47
msgid "Configuration of the 802.1X supplicant failed"
msgstr "Échec de la configuration du suppliant 802.1X"

#: ../../network/state_handler.go:48
msgid "The 802.1X supplicant quitted or failed unexpectedly"
msgstr "La supplication 802.1X a quitté ou a échoué de manière inattendue"

#: ../../network/state_handler.go:49
msgid "The 802.1X supplicant took too long time to authenticate"
msgstr "Le suppliant 802.1X a pris trop de temps pour s'authentifier"

#: ../../network/state_handler.go:50
msgid "The PPP service failed to start within the allowed time"
msgstr "Le service PPP n'a pas réussi à démarrer dans le temps alloué"

#: ../../network/state_handler.go:51
msgid "The PPP service disconnected unexpectedly"
msgstr "Le service PPP s'est déconnecté de manière inattendue"

#: ../../network/state_handler.go:52
msgid "The PPP service quitted or failed unexpectedly"
msgstr "Le service PPP a quitté ou a échoué de manière inattendue"

#: ../../network/state_handler.go:53
msgid "The DHCP service failed to start within the allowed time"
msgstr "Le service DHCP n'a pas pu démarrer dans le temps alloué"

#: ../../network/state_handler.go:54
msgid "The DHCP service reported an unexpected error"
msgstr "Le service DHCP a reporté une erreur inattendue"

#: ../../network/state_handler.go:55
msgid "The DHCP service quitted or failed unexpectedly"
msgstr "Le service DHCP a quitté ou a échoué de manière inattendue"

#: ../../network/state_handler.go:56
msgid "The shared connection service failed to start"
msgstr "Le service de connexion partagée n'a pas pu démarrer"

#: ../../network/state_handler.go:57
msgid "The shared connection service quitted or failed unexpectedly"
msgstr ""
"Le service de connexion partagée a quitté ou a échoué de manière inattendue"

#: ../../network/state_handler.go:58
msgid "The AutoIP service failed to start"
msgstr "Le service AutoIP n'a pas pu démarrer"

#: ../../network/state_handler.go:59
msgid "The AutoIP service reported an unexpected error"
msgstr "Le service AutoIP a reporté une erreur inattendue"

#: ../../network/state_handler.go:60
msgid "The AutoIP service quitted or failed unexpectedly"
msgstr "Le service AutoIP a quitté ou a échoué de manière inattendue"

#: ../../network/state_handler.go:61
msgid "Dialing failed due to busy lines"
msgstr "La composition a échoué car la ligne est actuellement occupée"

#: ../../network/state_handler.go:62
msgid "Dialing failed due to no dial tone"
msgstr "La composition a échoué dû à l'absence de tonalité"

#: ../../network/state_handler.go:63
msgid "Dialing failed due to the carrier"
msgstr "La composition a échoué dû à l'opérateur"

#: ../../network/state_handler.go:64
msgid "Dialing timed out"
msgstr "Délai de composition dépassé"

#: ../../network/state_handler.go:65
msgid "Dialing failed"
msgstr "Échec de la composition"

#: ../../network/state_handler.go:66
msgid "Modem initialization failed"
msgstr "Échec de l'initialisation du modem"

#: ../../network/state_handler.go:67
msgid "Failed to select the specified GSM APN"
msgstr "Échec de la sélection de l'APN GSM spécifié"

#: ../../network/state_handler.go:68
msgid "No networks searched"
msgstr "Aucun réseau recherché"

#: ../../network/state_handler.go:69
msgid "Network registration was denied"
msgstr "L'enregistrement sur le réseau a été refusé"

#: ../../network/state_handler.go:70
msgid "Network registration timed out"
msgstr "Le délai de l'enregistrement sur le réseau est dépassé"

#: ../../network/state_handler.go:71
msgid "Failed to register to the requested GSM network"
msgstr "Impossible de s'associer au réseau GSM demandé"

#: ../../network/state_handler.go:72
msgid "PIN check failed"
msgstr "Échec de la vérification du NIP"

#: ../../network/state_handler.go:73
msgid "Necessary firmware for the device may be missed"
msgstr "Le pilote requis pour le périphérique peut être manquant"

#: ../../network/state_handler.go:74
msgid "The device was removed"
msgstr "Le périphérique a été supprimé"

#: ../../network/state_handler.go:75
msgid "NetworkManager went to sleep"
msgstr "Le gestionnaire réseau s'est mis en veille"

#: ../../network/state_handler.go:76
msgid "The device's active connection was removed or disappeared"
msgstr "La connexion active du périphérique a été supprimée ou a disparue"

#: ../../network/state_handler.go:77
msgid "A user or client requested to disconnect"
msgstr "Un utilisateur ou un client a demandé à être déconnecté"

#: ../../network/state_handler.go:78
msgid "The device's carrier/link changed"
msgstr "La liaison du périphérique a été changée"

#: ../../network/state_handler.go:79
msgid "The device's existing connection was assumed"
msgstr "La commexion existante du périphérique a été fixée"

#: ../../network/state_handler.go:80
msgid "The 802.1x supplicant is now available"
msgstr "Le supplicant 802.1X est maintenant disponible"

#: ../../network/state_handler.go:81
msgid "The modem could not be found"
msgstr "Le modem n'a pas pu être trouvé"

#: ../../network/state_handler.go:82
msgid "The Bluetooth connection timed out or failed"
msgstr ""
"Le délai de la connexion Bluetooth est dépassé ou la connexion a échoué"

#: ../../network/state_handler.go:83
msgid "GSM Modem's SIM Card was not inserted"
msgstr "La carte SIM du modem GSM n'est pas insérée"

#: ../../network/state_handler.go:84
msgid "GSM Modem's SIM PIN required"
msgstr "Le NIP de la carte SIM du modem GSM est requis"

#: ../../network/state_handler.go:85
msgid "GSM Modem's SIM PUK required"
msgstr "Le code PUK de la carte SIM du modem GSM est requis"

#: ../../network/state_handler.go:86
msgid "SIM card error in GSM Modem"
msgstr "Erreur de carte SIM dans le modem GSM"

#: ../../network/state_handler.go:87
msgid "InfiniBand device does not support connected mode"
msgstr "Les périphériques InfiniBand ne supportent pas le mode connecté"

#: ../../network/state_handler.go:88
msgid "A dependency of the connection failed"
msgstr "Une dépendance de la connexion a échoué"

#: ../../network/state_handler.go:89
msgid "RFC 2684 Ethernet bridging error to ADSL"
msgstr "Erreur de pontage Ethernet RFC 2684 sur ADSL"

#: ../../network/state_handler.go:90
msgid "ModemManager did not run or quitted unexpectedly"
msgstr ""
"ModemManager n'est pas en cours d'exécution ou a quitté de manière "
"innatendue"

#: ../../network/state_handler.go:91
msgid "The 802.11 WLAN network could not be found"
msgstr "Le réseau WIFI 802.11 ne peut pas être trouvé"

#: ../../network/state_handler.go:92
msgid "A secondary connection of the base connection failed"
msgstr "Une connexion secondaire de la connexion principale a échoué"

#: ../../network/state_handler.go:95
msgid "DCB or FCoE setup failed"
msgstr "Configuration du DCB ou FCoE échouée"

#: ../../network/state_handler.go:96
msgid "Network teaming control failed"
msgstr "Échec de contrôle de l'association du réseau"

#: ../../network/state_handler.go:97
msgid "Modem failed to run or not available"
msgstr "Le modem n'a pas pu démarrer ou n'est pas disponible"

#: ../../network/state_handler.go:98
msgid "Modem now ready and available"
msgstr "Modem prêt"

#: ../../network/state_handler.go:99
msgid "SIM PIN is incorrect"
msgstr "Le NIP de la carte SIM est incorrect"

#: ../../network/state_handler.go:100
msgid "New connection activation is enqueuing"
msgstr "Activation de la nouvelle connexion mise en attente"

#: ../../network/state_handler.go:101
msgid "Parent device changed"
msgstr "Le périphérique parent a changé"

#: ../../network/state_handler.go:102
msgid "Management status of parent device changed"
msgstr "L'état de gestion du périphérique parent a changé"

#: ../../network/state_handler.go:105
msgid "Network cable is unplugged"
msgstr "Le câble réseau est débranché"

#: ../../network/state_handler.go:106
msgid "Please make sure SIM card has been inserted with mobile network signal"
msgstr ""
"Assurez-vous que la carte SIM a été insérée avec un signal du réseau mobile"

#: ../../network/state_handler.go:107
msgid ""
"Please make sure a correct plan was selected without arrearage of SIM card"
msgstr ""
"Veuillez-vous assurer qu'un plan correct a été sélectionné sans arriérés de "
"la carte SIM"

#: ../../network/state_handler.go:110
msgid "Failed to activate VPN connection, reason unknown"
msgstr "Impossible d'activer la connexion VPN, raison inconnue"

#: ../../network/state_handler.go:111
msgid "Failed to activate VPN connection"
msgstr "Impossible d'activer la connexion VPN"

#: ../../network/state_handler.go:112
msgid "The VPN connection state changed due to being disconnected by users"
msgstr ""
"L'état de la connnexion VPN a changé du fait d'une déconnexion due aux "
"utilisateurs"

#: ../../network/state_handler.go:113
msgid ""
"The VPN connection state changed due to being disconnected from devices"
msgstr ""
"L'état de la connnexion VPN a changé du fait d'une déconnexion des "
"périphériques"

#: ../../network/state_handler.go:114
msgid "VPN service stopped"
msgstr "Le service VPN s'est arrêté"

#: ../../network/state_handler.go:115
msgid "The IP config of VPN connection was invalid"
msgstr "La configuration IP de la connexion VPN était invalide"

#: ../../network/state_handler.go:116
msgid "The connection attempt to VPN service timed out"
msgstr "La tentative de connexion au service VPN a expiré"

#: ../../network/state_handler.go:117
msgid "The VPN service start timed out"
msgstr "Le démarrage du service VPN a expiré"

#: ../../network/state_handler.go:118
msgid "The VPN service failed to start"
msgstr "Le démarrage du service VPN a échoué"

#: ../../network/state_handler.go:119
msgid "The VPN connection password was not provided"
msgstr "Le mot de passe nécessaire à la connexion VPN n'a pas été fourni"

#: ../../network/state_handler.go:120
msgid "Authentication to VPN server failed"
msgstr "L'authentification au serveur VPN a échoué"

#: ../../network/state_handler.go:121
msgid "The connection was deleted from settings"
msgstr "La connexion a été supprimée des paramètres"

#: ../../network/state_handler.go:252
msgid "Enabling hotspot"
msgstr "Activation du point d'accès"

#: ../../network/state_handler.go:256
msgid "Connecting %q"
msgstr "Connexion %q"

#: ../../network/state_handler.go:265
msgid "Hotspot enabled"
msgstr "Point d'accès activé"

#: ../../network/state_handler.go:267
msgid "%q connected"
msgstr "%q connecté"

#: ../../network/state_handler.go:278 ../../network/state_handler.go:333
msgid "Hotspot disabled"
msgstr "Point d'accès désactivé"

#: ../../network/state_handler.go:341
msgid "Unable to share hotspot, please check dnsmasq settings"
msgstr ""
"Impossible de partager le point d'accès, veuillez vérifier les paramètres de"
" dnsmasq"

#: ../../network/state_handler.go:343
msgid "Unable to connect %q, please keep closer to the wireless router"
msgstr ""
"Impossible de se connecter à %q, veuillez-vous rapprocher du routeur sans "
"fil."

#: ../../network/state_handler.go:345
msgid "Unable to connect %q, please check your router or net cable."
msgstr ""
"Impossible de se connecter à %q, veuillez vérifier votre routeur ou votre "
"câble internet."

#: ../../network/state_handler.go:349
msgid "Connection failed, unable to connect %q, wrong password"
msgstr ""
"Échec de la connexion, impossible de connecter %q, mot de passe incorrect"

#: ../../network/state_handler.go:360
msgid "Password is required to connect %q"
msgstr "Mot de passe requis pour se connecter à %q"

#: ../../network/state_handler.go:362
msgid "The %q 802.11 WLAN network could not be found"
msgstr "Le réseau WLAN 802.11 %q est introuvable"

#: ../../network/utils_dbus_nm.go:256
msgid "Unknown"
msgstr "Inconnu"

#: ../../network/utils_notify.go:185
msgid "Airplane mode enabled."
msgstr "Mode avion activé."

#: ../../network/utils_notify.go:185 ../../network/utils_notify.go:189
#: ../../network/utils_notify.go:193 ../../network/utils_notify.go:211
#: ../../network/utils_notify.go:214 ../../network/utils_notify.go:303
msgid "Disconnected"
msgstr "Déconnecté"

#: ../../network/utils_notify.go:193
msgid "Access Point mode is not supported by this device."
msgstr "Le mode point d'accès n'est pas supporté par ce périphérique."

#: ../../network/utils_notify.go:197
msgid ""
"The hardware switch of WLAN Card is off, please switch on as necessary."
msgstr ""
"Le connecteur de la carte WIFI est désactivé, veuillez l'activer lorsque "
"nécessaire."

#: ../../network/utils_notify.go:201
msgid "System proxy is set successfully."
msgstr "Le proxy a été réglé avec succès."

#: ../../network/utils_notify.go:204
msgid "System proxy has been cancelled."
msgstr "Le proxy a été annulé."

#: ../../network/utils_notify.go:208
msgid "Connected"
msgstr "Connecté"

#: ../../service_trigger/msg.go:7
#, c-format
msgid "\"%s\" did not pass the system security verification, and cannot run now"
msgstr ""
"\"%s\" n'a pas passé la vérification de sécurité du système et ne peut pas "
"s'exécuter maintenant"

#: ../../session/power/manager_events.go:197
msgid "Battery critically low"
msgstr "Niveau de batterie critique"

#: ../../session/power/manager_events.go:218
#: ../../session/power/manager_events.go:223
#: ../../session/power/manager_events.go:228
msgid "Battery low, please plug in"
msgstr "Batterie faible, veuillez brancher votre appareil"

#: ../../session/power/utils.go:379 ../../session/power/utils.go:383
#: ../../session/power/utils.go:387 ../../session/power/utils.go:391
msgid "Power settings changed"
msgstr "Paramètres d'énergie changée"

#: ../../session/power/utils.go:406 ../../session/power/utils_test.go:12
#: ../../session/power/utils_test.go:13
msgid "When the lid is closed, "
msgstr "Lorsque le couvercle est fermé,"

#: ../../session/power/utils.go:410
msgid "When pressing the power button, "
msgstr "Lorsque vous appuyez sur le bouton d'alimentation,"

#: ../../session/power/utils.go:420 ../../session/power/utils_test.go:12
msgid "your computer will shut down"
msgstr "votre ordinateur va s'éteindre"

#: ../../session/power/utils.go:422 ../../session/power/utils_test.go:13
msgid "your computer will suspend"
msgstr "L'ordinateur se mettra en veille "

#: ../../session/power/utils.go:424
msgid "your computer will hibernate"
msgstr "votre ordinateur se mettra en veille prolongée"

#: ../../session/power/utils.go:426
msgid "your monitor will turn off"
msgstr "votre écran s'éteindra"

#: ../../session/power/utils.go:428
msgid "it will do nothing to your computer"
msgstr "ne rien faire à l'ordinateur"

#: ../../timedate/manager_ifc.go:66
msgid "Authentication is required to set the system time"
msgstr "Identification requise pour paramétrer l'heure du système"

#: ../../timedate/manager_ifc.go:79
msgid ""
"Authentication is required to control whether network time synchronization "
"shall be enabled"
msgstr ""
"Identification requise pour déterminer si la synchronisation de l'heure "
"réseau doit être activée."

#: ../../timedate/manager_ifc.go:89
msgid "Authentication is required to change NTP server"
msgstr "L'authentification est nécessaire pour changer de serveur NTP"

#: ../../timedate/manager_ifc.go:129
msgid ""
"Authentication is required to control whether the RTC stores the local or "
"UTC time"
msgstr ""
"Identification requise pour déterminer si le RTC stocke l'heure locale ou "
"UTC"

#: ../../timedate/manager_ifc.go:148
msgid "Authentication is required to set the system timezone"
msgstr "Identification requise pour paramétrer le fuseau horaire du système"
